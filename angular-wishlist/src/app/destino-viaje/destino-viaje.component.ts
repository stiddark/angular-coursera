import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output, Input, HostBinding } from '@angular/core';

import { DestinosViajes } from '../models/destino-viajes.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { from } from 'rxjs';
import {
  VoteUpAction,
  VoteDownAction,
} from '../models/destino-viajes-states.model.';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinosViajes;
  @Input('idx') posicion: number;
  @HostBinding('attr.class') cssClass = 'col-md-4 mt-2';
  @Output() clicked: EventEmitter<DestinosViajes>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {}

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
