import { Injectable } from '@angular/core';
import { DestinosViajes } from '../models/destino-viajes.model';
import { Store } from '@ngrx/store';
import {
  NuevoDestinoAction,
  ElegidoFavoritoAction,
} from '../models/destino-viajes-states.model.';
import { AppState } from './../app.module';

@Injectable()
export class DestinosApiClient {
  constructor(private store: Store<AppState>) {}

  add(d: DestinosViajes) {
    //aqui incovariamos al servidor
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegir(d: DestinosViajes) {
    //aqui incovariamos al servidor
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
